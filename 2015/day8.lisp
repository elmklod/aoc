(setf total-diff 0)

;;; double quotes from the input are also considered characters of the string being proccessed.
(defun compute-diff ()
  (setf total-diff 0)
  (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
    (do ((string (read-line stream nil) (read-line stream nil))
            (diff 2 2))
          ((null string) total-diff)
      (do ((string-pos 1) ;;; skip the first double quote
             (string-end (- (length string) 1)))
            ((= string-pos string-end) (incf total-diff diff)) ;;; skip the last quote
        (if (eql (char string string-pos) #\\)
            (cond ((or (eql (char string (+ string-pos 1)) #\\)
                             (eql (char string (+ string-pos 1)) #\"))
                        (incf string-pos 2)
                        (incf diff))
                       (t (incf string-pos 4) (incf diff 3)))
            (incf string-pos))))))


;;;;;


(defun compute-diff ()
  (setf total-diff 0)
  (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
    (do ((string (read-line stream nil) (read-line stream nil))
            (diff 4 4))
          ((null string) total-diff)
      (do ((string-pos 1) ;;; skip the first quote
             (string-end (- (length string) 1)))
            ((= string-pos string-end) (incf total-diff diff)) ;;; skip the last quote
        (if (or (eql #\\ (char string string-pos))
                        (eql #\" (char string string-pos)))
            (incf diff))
        (incf string-pos)))))