(defun look-and-say (string)
  (labels ((iter (string new-string pos char times)
                 (if (= pos (length string))
                     (nreverse (concatenate 'string (string char) (write-to-string times) new-string))
                     (progn
                       (if (eql (char string pos) char)
                           (iter string new-string (+ pos 1) char (+ times 1))
                           (iter string (concatenate 'string (string char) (write-to-string times) new-string) (+ pos 1) (char string pos) 1))))))
    (dotimes (i 40 (length string))
      (setf string (iter string "" 1 (char string 0) 1)))))



(defun look-and-say (string)
  (labels ((iter (string new-string pos char times)
                 (if (= pos (length string))
                     (nreverse (concatenate 'string (string char) (write-to-string times) new-string))
                     (progn
                       (if (eql (char string pos) char)
                           (iter string new-string (+ pos 1) char (+ times 1))
                           (iter string (concatenate 'string (string char) (write-to-string times) new-string) (+ pos 1) (char string pos) 1))))))
    (dotimes (i 50 (length string))
      (setf string (iter string "" 1 (char string 0) 1)))))

;;; fastest
(defun look-and-say (string n)
  (labels ((iter (string new-string pos char times)
                 (if (= pos (length string))
                     (coerce (nreverse (cons char (nconc (coerce (write-to-string times) 'list) new-string))) 'string)
                     (progn
                       (if (eql (char string pos) char)
                           (iter string new-string (+ pos 1) char (+ times 1))
                           (iter string (cons char (nconc (coerce (write-to-string times) 'list) new-string)) (+ pos 1) (char string pos) 1))))))
    (dotimes (i n (length string))
      (setf string (iter string nil 1 (char string 0) 1)))))


;;; on par or even better(or a little worse)
(defun look-and-say (number n)
  (setf number (coerce (write-to-string number) 'list))
  (labels ((iter (number-char-list new-number-char-list char times)
                 (if (null number-char-list)
                     (nreverse (cons char (nconc (coerce (write-to-string times) 'list) new-number-char-list)))
                     (progn
                       (if (eql (car number-char-list) char)
                           (iter (cdr number-char-list) new-number-char-list char (+ times 1))
                           (iter (cdr number-char-list) (cons char (nconc (coerce (write-to-string times) 'list) new-number-char-list)) (car number-char-list) 1))))))
    (dotimes (i n (length number))
      (setf number (iter (cdr number) nil (car number) 1)))))
