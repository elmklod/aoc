;;; the way numbers are generated forms equilateral triangles sharing the same angle
;;; for each triangle its diagonal side has length that is equal to the row its lowest vertex sits at.
;;; it's by definition, row number during generation of the side diminishes up to 1
;;; while column number grows from 1 to the initial row number.
;;; therefore, each side has length of lowest point row number, but for each row only one diagonal side rests on it
;;; continiuously generating numbers eventually moves to the next diagonal side.
;;; Each side is greater than the previous by one.
;;; The initial side is of length 1.
;;; it takes exactly the length of the diagonal side to move from the beginning of the side to the next side beginning
;;; the input given requires the number from some column and row.
;;; find how many steps it takes from the first column to the given
;;; this is how many times row number decreased.
;;; thefore, the initial row is = row + (column - 1)
;;; thefore, the number is located on the side that has length of this initial row and is the-initial-rowth side
;;; Combine every side number before initial-row side.
;;; there are initial-row - 1 sides.
;;; sum of arithmetic progression
;;; (initial-row - 1 + 1)*(initial-row - 1)/2
;;; that many iterations transform the number to the first number on the initial row side
;;; It also takes column - 1 iterations to reach the number from the first number on the side as per algorithm
;;; therefore, it takes (initial-row - 1 + 1)*(initial-row - 1)/2 + column - 1 iterations overall


(setf n 20151125)

(dotimes (i (+ 3018 (* 3014 6027)) n)
  (setf n (rem (* n 252533) 33554393)))


(setf num 20151125)
(setf column 3019)
(setf row 3010)
(setf initial-row (+ row (- column 1)))
(setf iterations (+ (- column 1)
                             (/ (* (+ (- initial-row 1) 1)
                                    (- initial-row 1))
                                2)))

(dotimes (i iterations num)
 (setf num (rem (* num 252533) 33554393)))

(defun iter (num i)
 (if (> i iterations)
     num
     (iter (rem (* num 252533) 33554393) (+ 1 i)))) 
;numbers here are what was specified in the instruction to get the next

(iter num 1)
