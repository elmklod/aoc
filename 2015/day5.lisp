(defun count-nice ()
  (let ((nice 0))
    (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
      (do ((string (read-line stream nil) (read-line stream nil)))
             ((null string) nice)
        (if (and (not (search "ab" string))
                     (not (search "cd" string))
                     (not (search "pq" string))
                     (not (search "xy" string))
                     (duplicates-p string)
                     (enough-vowels-p string))
            (incf nice))))))

(defun duplicates-p (string)
  (let ((max (- (length string) 1)))
    (labels ((rec (p)
                  (cond ((not (= p max))
                              (let ((p-next (+ 1 p)))
                                (if (eql (elt string p)
                                           (elt string p-next))
                                    t
                                    (rec p-next))))
                             (t nil))))
            (rec 0))))

(defun enough-vowels-p (string)
  (>= (length
          (remove 
           nil
           (map 'list (lambda (char) (find char "aeiou")) string)))
         3))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun count-nice ()
  (let ((nice 0))
    (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
      (do ((string (read-line stream nil) (read-line stream nil)))
             ((null string) nice)
        (if (and (letter-pairs-duplicates-p string)
                     (repeats-p string))
            (incf nice))))))

(defun repeats-p (string)
  (let ((max (- (length string) 1)))
    (labels ((rec (p)
                  (cond ((not (< (- max p) 2))
                              (let ((p-next (+ 2 p)))
                                (if (eql (elt string p)
                                           (elt string p-next))
                                    t
                                    (rec (+ 1 p))))) ; necessary to check the letter between
                             (t nil))))
            (rec 0))))

(defun letter-pairs-duplicates-p (string)
  (let ((end (- (length string) 1)))
    (do ((start 0 (+ 1 start)))
           ((< (- end start) 3) nil)
      (let ((string-start (+ start 2)))
        (if (search (subseq string start string-start)
                          string
                          :start2 string-start)
            (return t))))))
