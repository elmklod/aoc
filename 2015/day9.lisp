(setf distances nil)

(defun make-perm (el-list)
  (labels ((make-perm-rec (perm-list)
 (if (and (not (null perm-list))
              (= (length el-list)
                  (length (first perm-list))))
     perm-list
     (make-perm-rec
       (reduce #'append
        (mapcar
       (lambda (e)
          (mapcar
           (lambda (perm)
             (cons e perm))
           (remove-if
            (lambda (perm) (member e perm :test #'string-equal))
            perm-list)))
       el-list))))))
    (make-perm-rec (mapcar #'list el-list))))

(defun read-distances ()
  (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
    (do ((instruction (read-line stream nil) (read-line stream nil)))
          ((null instruction))
      (let* ((first-town-end-pos (search " to" instruction))
              (second-town-end-pos (search " =" instruction))
              (first-town (subseq instruction 0 first-town-end-pos))
              (second-town (subseq instruction (+ first-town-end-pos 4) second-town-end-pos))
              (distance (parse-integer (subseq instruction (+ second-town-end-pos 3))))
              (first-entry (assoc first-town distances :test #'string-equal))
              (second-entry (assoc second-town distances :test #'string-equal)))
        (if (null first-entry)
            (push (list first-town (list second-town distance)) distances)
            (setf (cdr first-entry) (cons (list second-town distance) (cdr first-entry))))
        (if (null second-entry)
            (push (list second-town (list first-town distance)) distances)
            (setf (cdr second-entry) (cons (list first-town distance) (cdr second-entry))))))))

(defun find-smallest ()
  (reduce #'min (mapcar #'compute-dist (make-perm (mapcar #'car distances)))
          :initial-value 1000000000)) ;;; choose constant that is certainly greater.

(defun compute-dist (town-list)
  (labels ((rec (dist town-list)
                 (if (null (cdr town-list))
                     dist
                     (rec (+ dist
                                (cadr (assoc (second town-list)
                                                      (cdr (assoc (first town-list) distances :test #'string-equal))
                                                      :test #'string-equal)))
                            (cdr town-list)))))
    (rec 0 town-list)))

(defun find-greatest ()
  (reduce #'max (mapcar #'compute-dist (make-perm (mapcar #'car distances)))
          :initial-value 0))