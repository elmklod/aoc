(defun bulbs-solve ()
 (let ((bulbs (make-array '(1000 1000) :initial-element nil))
         (read-pos nil)
         (f nil))
  (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
   (do  ((command (read-line stream nil) (read-line stream nil)))
          ((null command) (do* ((x 0)
                                               (y 0)
                                               (count (if (aref bulbs x y)
                                                               1
                                                               0) 
                                                           (+ count (if (aref bulbs x y)
                                                                              1
                                                                              0))))
                                              ((= x y 999) count)
                                              (if (= x 999)
                                                  (setf x 0
                                                          y (+ 1 y))
                                                  (incf x))))
   (let ((type (char command 6)))
     (cond ((eql type #\n)
                 (setf read-pos 8
                         f (lambda (x y)
                             (setf (aref bulbs x y) t))))
               ((eql type #\space)
                (setf read-pos 7
                        f (lambda (x y)
                            (setf (aref bulbs x y) (not (aref bulbs x y))))))
               ((eql type #\f)
                (setf read-pos 9
                        f (lambda (x y)
                            (setf (aref bulbs x y) nil)))))
     (multiple-value-bind (first-x disp-read-pos) (parse-integer command :junk-allowed t :start read-pos)
       (multiple-value-bind (first-y disp-read-pos) (parse-integer command :junk-allowed t :start (+ disp-read-pos 1))
         (multiple-value-bind (last-x disp-read-pos) (parse-integer command :junk-allowed t :start (+ disp-read-pos 9))
           (let ((last-y (parse-integer command :junk-allowed t :start (+ disp-read-pos 1))))
             (do ((x first-x)
                     (y first-y))
                   ((> x last-x))
               (funcall f x y)
               (if (and (= x last-x)
                            (< y last-y))
                   (setf x first-x
                           y (+ 1 y))
                   (incf x))))))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


                   
(defun bulbs-solve ()
 (let ((bulbs (make-array '(1000 1000) :initial-element 0))
         (read-pos nil)
         (f nil))
  (with-open-file (stream (make-pathname :device "D" :directory "" :name "input" :type "txt") :direction :input)
   (do  ((command (read-line stream nil) (read-line stream nil)))
          ((null command) (do* ((x 0)
                                               (y 0)
                                               (count (aref bulbs x y)
                                                           (+ count (aref bulbs x y))))
                                              ((= x y 999) count)
                                              (if (= x 999)
                                                  (setf x 0
                                                          y (+ 1 y))
                                                  (incf x))))
   (let ((type (char command 6)))
     (cond ((eql type #\n)
                 (setf read-pos 8
                         f (lambda (x y)
                             (incf (aref bulbs x y)))))
               ((eql type #\space)
                (setf read-pos 7
                        f (lambda (x y)
                            (incf (aref bulbs x y) 2))))
               ((eql type #\f)
                (setf read-pos 9
                        f (lambda (x y)
                            (if (= 0 (aref bulbs x y))
                                0
                                (decf (aref bulbs x y)))))))
     (multiple-value-bind (first-x disp-read-pos) (parse-integer command :junk-allowed t :start read-pos)
       (multiple-value-bind (first-y disp-read-pos) (parse-integer command :junk-allowed t :start (+ disp-read-pos 1))
         (multiple-value-bind (last-x disp-read-pos) (parse-integer command :junk-allowed t :start (+ disp-read-pos 9))
           (let ((last-y (parse-integer command :junk-allowed t :start (+ disp-read-pos 1))))
             (do ((x first-x)
                     (y first-y))
                   ((> x last-x))
               (funcall f x y)
               (if (and (= x last-x)
                            (< y last-y))
                   (setf x first-x
                           y (+ 1 y))
                   (incf x))))))))))))
