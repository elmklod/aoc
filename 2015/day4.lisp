;before loading md5 code in sbcl, load this module.
(require :sb-rotate-byte)

; blows the stack on lispworks although it can be get tail-call optimization.
; does not blow it on sbcl
(defun find-md5 (string)
  (labels ((rec (number)
                 (let* ((input (concatenate 'string string (write-to-string number :base 10)))
                          (hash (md5sum-string input)))
                   (if (and (= 0 (elt hash 0)) (= 0 (elt hash 1))
                                (< (elt hash 2) 16))
                       number
                       (rec (+ number 1))))))
    (rec 1)))

;;;;;;;;;;;;;;;

(defun find-md5 (string)
  (labels ((rec (number)
                 (let* ((input (concatenate 'string string (write-to-string number :base 10)))
                          (hash (md5sum-string input)))
                   (if (and (= 0 (elt hash 0)) (= 0 (elt hash 1))
                                (= (elt hash 2) 0))
                       number
                       (rec (+ number 1))))))
    (rec 1))) ;;; one can use the answer of the first part as a starting point to speed it up a little
